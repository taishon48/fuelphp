<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>ログイン</title>
	<style>
  	body{margin:40px;}
  </style>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
	<div class="row">
	 <h1>ログイン</h1>
	<?php echo Form::open(array("action"=>'vote/login',"class"=>"form-group"));?>
	ユーザーID<?php echo Form::input('id','',array("class"=>"form-control")); ?><br>
	パスワード<?php echo Form::password('password','',array("class"=>"form-control")); ?><br>
	<?php echo Form::submit('login','ログイン',array("class"=>"btn btn-primary")); ?>
	<?php echo Form::close(); ?>
</div>
</div>
</body>
</html>
