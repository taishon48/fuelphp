<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>一覧</title>
	<style>
  body{
  	margin:40px;
	}
		ul{
			list-style: none;
		}
		img{
			width: 100%;
		}
		img:hover{

		}
		.flex h1{
			font-family: ヒラギノ角ゴ Pro W6;
			margin-bottom: 40px;
		}
		.flex{
			display: flex;
			-webkit-justify-content: space-between; /* Safari */
justify-content:         space-between;
		}
		.votes{
			width: 100%;
			display: block;
			text-align: center;
			color: #088;
			font-size: 50px;
		}
		.logout{
			display: block;
			padding: 10px;
			margin-top: 16px;
			color:#fff;
			background-color:#d00;
			border-radius: 6px;
			vertical-align: bottom;
			height:40px;
		}
  </style>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script
	  src="https://code.jquery.com/jquery-2.2.4.js"
	  integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
	  crossorigin="anonymous">
	</script>
	<script type="text/javascript">
		$(function(){
			$("button.vote").on('click',function(){
				var id =$(this).data('id');
				$.ajax({
					url:"<?php echo Uri::create('api/vote.json');?>",
					type:"POST",
					data:{id:id},
					dataType:"json"
				}).done(function(data){
					alert(data.message);
					location.reload();
				}).fail(function(data){
					alert("失敗");
				});
			});
		});
	</script>
</head>
<body>
	<div class="container">
	<div class="row">
<div class="flex">
  <h1>第9回たいしょん選抜総選挙立候補者一覧</h1>
	<?php echo Html::anchor('vote/logout','ログアウト',array('class'=>"logout")); ?>
</div>
	<ul>
	<?php foreach($images as $img):?>
		<li class="col-md-4"><?php echo Asset::img($img['file_name']);?>
			<span class="votes">得票数：<?php echo $img['votes']?></span>
			<button type="button" class="vote btn btn-primary btn-lg" data-id="<?php echo $img["id"];?>">投票する</button>
		</li>
	<?php endforeach;?>
	</ul>
	</div>
</div>
</body>
</html>
