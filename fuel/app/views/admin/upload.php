<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>アップロード画面</title>
	<style>
  	body{margin:40px;}
  </style>
</head>
<body>
  <h1>アップロード画面</h1>
  <?php echo Html::anchor('admin/logout','ログアウト'); ?>
  <?php echo Html::anchor('admin/view','一覧'); ?>
  <?php echo Form::open(array('action'=>'admin/upload','method'=>'post','enctype'=>'multipart/form-data')); ?>
  <?php echo Form::file('image'); ?>
  <?php echo Form::submit('upload','アップロード'); ?>
  <?php echo Form::close(); ?>

</body>
</html>
