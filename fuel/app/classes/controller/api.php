<?php

class Controller_Api extends Controller_Rest
{
  public function post_vote(){
    // ログインされているかチェック
    if(Auth::check()){
      // 投票数に+1
      $id = Input::post('id');
      $image = Model_Image::find($id);
      $image->votes = $image->votes+1;
      $image->save();
    }
    // 結果を返す
    return $this->response(
      array('message'=>'処理終了')
    );
  }
}
